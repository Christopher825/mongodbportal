package my.web.portal.dao;

import my.web.portal.model.Base;


/**
 * Created by christopherloganathan on 6/14/15.
 */
public interface BaseDao {



    void create(Base model);

    void delete(Base model);

    void update(Base model);

    void deleteAll(Class clazz);
}
