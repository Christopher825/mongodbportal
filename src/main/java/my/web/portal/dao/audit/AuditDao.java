package my.web.portal.dao.audit;

import my.web.portal.model.audit.Audit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Created by christopherloganathan on 15/03/2016.
 */
public interface AuditDao {

    void create(Audit audit);

    List<Audit> list();

    Page<Audit> list(Pageable pageable,String startDate,String endDate);
}
