package my.web.portal.dao.audit;

import my.web.portal.dao.BaseDaoImpl;
import my.web.portal.model.audit.Audit;
import my.web.portal.utils.UtilDates;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

/**
 * Created by christopherloganathan on 15/03/2016.
 */
@Repository
public final class AuditDaoImpl extends BaseDaoImpl implements AuditDao{

    @Override
    public void create(Audit audit) {

        mongoTemplate.insert(audit);
    }


    @Override
    public List<Audit> list() {

        return mongoTemplate.findAll(Audit.class);
    }
    @Override
    public Page<Audit> list(Pageable pageable,String startDate,String endDate) {

        Date start = UtilDates.toDateTime(startDate,UtilDates.auditDateFormat);
        Date end = UtilDates.toDateTime(endDate,UtilDates.auditDateFormat);

        Query query = new Query();
        Criteria criteria = Criteria.where("createdOn").gte(start).andOperator(Criteria.where("createdOn").lte(end));
        query.addCriteria(criteria);
        query.with(pageable);

        List<Audit> auditList = mongoTemplate.find(query, Audit.class);

        long total = mongoTemplate.count(query, Audit.class);
        Page<Audit> listPage = new PageImpl<Audit>(auditList,pageable,total);

        return listPage;
    }


}
