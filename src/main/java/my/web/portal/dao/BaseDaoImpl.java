package my.web.portal.dao;

import my.web.portal.model.Base;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;



/**
 * Created by christopherloganathan on 6/14/15.
 */
public abstract class BaseDaoImpl implements BaseDao{


    @Autowired
    protected MongoTemplate mongoTemplate;

    @Override
    public void create(Base model)
    {

        mongoTemplate.insert(model);
    }

    @Override
    public void delete(Base model)
    {
        mongoTemplate.remove(model);
    }

    @Override
    public void deleteAll(Class clazz)
    {
        mongoTemplate.dropCollection(clazz);
    }

    @Override
    public void update(Base model)
    {
        mongoTemplate.save(model);
    }




}
