package my.web.portal.dao.user;

import my.web.portal.dao.BaseDaoImpl;
import my.web.portal.model.user.User;
import my.web.portal.utils.CommonUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Repository
public final class UserDaoImpl extends BaseDaoImpl implements UserDetailsService,UserDao {

    @Override
    public void create(User user) {

        mongoTemplate.insert(user);
    }

    @Override
    public void delete(User user) {
        mongoTemplate.remove(user);
    }

    @Override
    public void update(User user) {
        mongoTemplate.save(user);
    }

    @Override
    public List<User> list() {

        return mongoTemplate.findAll(User.class);
    }

    public Page<User> list(Pageable pageable,String excludeAdminUserLogin,String search) {

        Query query = new Query();
        Criteria criteria = new Criteria().orOperator(
                Criteria.where("username").ne(excludeAdminUserLogin).orOperator(

                        Criteria.where("username").regex(Pattern.compile(CommonUtils.getDefaultStringValue(search,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
                        Criteria.where("name.first").regex(Pattern.compile(CommonUtils.getDefaultStringValue(search,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
                        Criteria.where("name.last").regex(Pattern.compile(CommonUtils.getDefaultStringValue(search,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
                        Criteria.where("email").regex(Pattern.compile(CommonUtils.getDefaultStringValue(search,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
                        Criteria.where("role").regex(Pattern.compile(CommonUtils.getDefaultStringValue(search,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
                        Criteria.where("status").regex(Pattern.compile(CommonUtils.getDefaultStringValue(search,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE))

                )
        );
        query.addCriteria(criteria);
        query.with(pageable);

        List<User>  userList = mongoTemplate.find(query, User.class);

        long total = mongoTemplate.count(query, User.class);
        Page<User> listPage = new PageImpl<User>(userList,pageable,total);

        return listPage;
    }



    @Override
    public User findUserByUsername(String username) {
        Query query = new Query();
        query.addCriteria(Criteria.where("username").regex(Pattern.compile(CommonUtils.getDefaultStringValue(username,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));
        return mongoTemplate.findOne(query, User.class);
    }


    @Override
    public User findUserByID(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongoTemplate.findOne(query, User.class);
    }

    @Override
    public User findUserByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").regex(Pattern.compile(CommonUtils.getDefaultStringValue(email,""), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));
        return mongoTemplate.findOne(query, User.class);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = findUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user found with username: " + username);
        }
        return user;
    }

}
