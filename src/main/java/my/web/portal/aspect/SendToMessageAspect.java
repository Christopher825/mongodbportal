package my.web.portal.aspect;

import my.web.portal.annotation.SendToMessage;
import my.web.portal.controller.AbstractController;
import my.web.portal.service.jms.JmsMessageSender;
import my.web.portal.utils.CommonUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import java.lang.reflect.Method;

/**
 * Created by christopherloganathan on 8/21/15.
 */
@Configurable
@Aspect
public final class SendToMessageAspect extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(SendToMessageAspect.class);

    @Autowired
    private JmsMessageSender jmsMessageSender;


    @After(value = "@annotation(my.web.portal.annotation.SendToMessage)")
    public void after(JoinPoint joinPoint) {


        for (Method m : joinPoint.getTarget().getClass().getMethods()) {
            SendToMessage sendToMessage = m.getAnnotation(SendToMessage.class);
            if (!CommonUtils.isEmpty(sendToMessage)) {
                if (CommonUtils.isEmpty(sendToMessage.destination())) {
                    jmsMessageSender.send(sendToMessage.text());
                } else {
                    jmsMessageSender.send(sendToMessage.destination(), sendToMessage.text());
                }
                break;
            }
        }
    }
}
