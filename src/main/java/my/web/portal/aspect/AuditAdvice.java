package my.web.portal.aspect;

import my.web.portal.annotation.AuditTrail;
import my.web.portal.controller.AbstractController;
import my.web.portal.model.audit.Audit;
import my.web.portal.model.user.User;
import my.web.portal.service.audit.AuditService;
import my.web.portal.utils.AuthenticationUtils;
import my.web.portal.utils.CommonUtils;
import my.web.portal.utils.UtilDates;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;


/**
 * Created by christopherloganathan on 15/03/2016.
 */
@Configurable
@Aspect
public final class AuditAdvice extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(AuditAdvice.class);

    @Autowired
    private AuditService auditService;

    @Before("execution(* my.web.portal.controller.home.*Controller.*(..)) && @annotation(auditTrail) ")
    public void auditHome(AuditTrail auditTrail) {

        User user = AuthenticationUtils.getAuthenticatedUser();
        String username = CommonUtils.isEmpty(user)?"anonymous":user.getUsername();
        String accessLevel= CommonUtils.isEmpty(user)?getCurrentRoleFromSecurityConfig(""):getRoles(user.getAuthorities()).toString();
        logger.info("Audit Advice Access Name::::::" + auditTrail.value());
        Audit audit= new Audit();
        audit.setUsername(username);
        audit.setAccessLevel(accessLevel);
        audit.setAction(auditTrail.value());
        audit.setCreatedOn(UtilDates.currentDateTime());
        auditService.create(audit);
    }

    @Before("execution(* my.web.portal.controller.users.*Controller.*(..)) && @annotation(auditTrail) ")
    public void auditUser(AuditTrail auditTrail) {

        User user = AuthenticationUtils.getAuthenticatedUser();
        String username = CommonUtils.isEmpty(user)?"anonymous":user.getUsername();
        String accessLevel= CommonUtils.isEmpty(user)?getCurrentRoleFromSecurityConfig(""):getRoles(user.getAuthorities()).toString();
        logger.info("Audit Advice Access Name::::::" + auditTrail.value());
        Audit audit= new Audit();
        audit.setUsername(username);
        audit.setAccessLevel(accessLevel);
        audit.setAction(auditTrail.value());
        audit.setCreatedOn(UtilDates.currentDateTime());
        auditService.create(audit);
    }

    @Before("execution(* my.web.portal.handlers.*Handler.*(..)) && @annotation(auditTrail) ")
    public void auditHandlers(AuditTrail auditTrail) {

        User user = AuthenticationUtils.getAuthenticatedUser();
        String username = CommonUtils.isEmpty(user)?"anonymous":user.getUsername();
        String accessLevel= CommonUtils.isEmpty(user)?getCurrentRoleFromSecurityConfig(""):getRoles(user.getAuthorities()).toString();
        logger.info("Audit Advice Access Name::::::" + auditTrail.value());
        Audit audit= new Audit();
        audit.setUsername(username);
        audit.setAccessLevel(accessLevel);
        audit.setAction(auditTrail.value());
        audit.setCreatedOn(UtilDates.currentDateTime());
        auditService.create(audit);
    }

    private StringBuffer getRoles(Collection<? extends  GrantedAuthority> grantedAuthorities){
        StringBuffer sb = new StringBuffer();
        return sb.append(getCurrentRoleFromSecurityConfig(grantedAuthorities.iterator().next().getAuthority()));
    }
}
