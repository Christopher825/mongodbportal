package my.web.portal.aspect;

import my.web.portal.utils.ExceptionUtil;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

/**
 * Created by christopherloganathan on 6/24/15.
 */

@Aspect
public final class ExceptionAspectProcessor {

    @AfterReturning(
            pointcut = "execution(* my.web.portal.utils.ExceptionProcessor.process(..))",
            returning = "exception")
    public void logAfterReturningCaughtException(Exception exception){
        ExceptionUtil.handleAllCaughtException(exception);

    }

}
