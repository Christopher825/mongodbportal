package my.web.portal.error;

import com.google.common.base.Throwables;
import my.web.portal.utils.CommonUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;

/**
 * Created by christopherloganathan on 6/16/15.
 * @handle Display an error page, as defined in web.xml <code>custom-error</code> element.
 */
@Controller
public class CustomErrorController {

	@RequestMapping("generalError")
	public String generalError(HttpServletRequest request, Model model) {
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		if (CommonUtils.isEmpty(requestUri))
			requestUri = "Unknown";


		String message = MessageFormat.format("{0} returned for {1} with message {2}",
			statusCode, requestUri, exceptionMessage
		);

		model.addAttribute("errorMessage", message);
        return "error/error";
	}

	@RequestMapping("badRequest")
	public String badRequest(HttpServletRequest request, Model model) {


		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		if (CommonUtils.isEmpty(requestUri))
			requestUri = "Unknown";


		String message = MessageFormat.format("{0} returned for {1} with message {2}",
				400, request.getHeader("referer"), "Bad Request"
		);

		model.addAttribute("errorMessage", message);
		return "error/error";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (!CommonUtils.isEmpty(throwable))
			return Throwables.getRootCause(throwable).getMessage();

		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}
}
