package my.web.portal.config;

import my.web.portal.handlers.CustomAuthenticationFailureHandler;
import my.web.portal.handlers.CustomAuthenticationSuccessHandler;
import my.web.portal.handlers.CustomLogoutHandler;
import my.web.portal.dao.user.UserDao;
import my.web.portal.dao.user.UserDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Configuration
@ImportResource(value = "classpath:spring-security-context.xml")
public class SecurityConfig{


    @Bean
    public UserDao userDao() {
        return new UserDaoImpl();
    }

    @Bean
    public TokenBasedRememberMeServices rememberMeServices() {
        return new TokenBasedRememberMeServices("remember-me-key",(UserDaoImpl) userDao());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler(){
        return new CustomAuthenticationSuccessHandler();
    }
    @Bean
    public CustomAuthenticationFailureHandler customAuthenticationFailureHandler(){
        return new CustomAuthenticationFailureHandler();
    }

    @Bean
    public CustomLogoutHandler customLogoutHandler(){

        return new CustomLogoutHandler("/");
    }

    @Bean
    public RequestMatcher csrfSecurityRequestMatcher() {
        RequestMatcher csrfRequestMatcher = new RequestMatcher() {

            private final Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
            private final AntPathRequestMatcher[] requestMatchers = {
                    new AntPathRequestMatcher("/login"),
                    new AntPathRequestMatcher("/logout"),
                    new AntPathRequestMatcher("/**/test*")
            };


            @Override
            public boolean matches(HttpServletRequest request) {
                // ByPass CSRF for the allowed methods
                if(allowedMethods.matcher(request.getMethod()).matches())
                    return false;

                // ByPass CSRF for selected api calls
                for(AntPathRequestMatcher antPathRequestMatcher:requestMatchers) {
                    if (antPathRequestMatcher.matches(request))
                        return false;
                }

                return true;
            }

        };
        return csrfRequestMatcher;
    }

}
