package my.web.portal.config;

import my.web.portal.service.jms.JmsMessageListener;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQPrefetchPolicy;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import javax.jms.Connection;

/**
 * Created by christopherloganathan on 7/14/15.
 */
@Configuration
public class JMSConfig {

    private static final Logger logger = LoggerFactory.getLogger(JMSConfig.class);


    @Value("${jms.host}")
    private String jmsHost;

    @Value("${jms.test.send}")
    private String jmsTestSend;

    @Value("${jms.maxConnections}")
    private int jmsMaxConnections;

    @Value("${jms.maximumActiveSessionPerConnection}")
    private int jmsMaximumActiveSessionPerConnection;


    @Autowired
    private ThreadPoolTaskExecutor taskExecutorListener;

    @Autowired
    private JmsMessageListener jmsMessageListener;

    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory(){
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory("failover:(" +jmsHost + ")");
        ActiveMQPrefetchPolicy activeMQPrefetchPolicy = new ActiveMQPrefetchPolicy();
        activeMQPrefetchPolicy.setQueuePrefetch(0);
        activeMQPrefetchPolicy.setTopicPrefetch(0);
        activeMQConnectionFactory.setPrefetchPolicy(new ActiveMQPrefetchPolicy());
        logger.debug("Broker URL ::: " + activeMQConnectionFactory.getBrokerURL());
        return activeMQConnectionFactory;
    }


    @Bean
    public ActiveMQQueue activeMQQueueJMSTest() {
        return new ActiveMQQueue(jmsTestSend);
    }


    @Bean
    public CachingConnectionFactory cachingConnectionFactory(){
        return new CachingConnectionFactory(pooledConnectionFactory());
    }

    @Bean
    public ActiveMQQueue activeMQQueue(){
       return new ActiveMQQueue(jmsTestSend);
    }

    @Bean
    public JmsTemplate jmsTemplate(){
        JmsTemplate jms = new JmsTemplate();
        jms.setDefaultDestination(activeMQQueue());
        jms.setConnectionFactory(cachingConnectionFactory());
        return jms;
    }

    @Bean
    public Connection checkJMSConnection() throws Exception{
        Connection connection  = cachingConnectionFactory().createConnection();
        connection.start();
        logger.info("Connection test::::[OK]");
        connection.close();
        logger.info("Connection test::::[Close]");
        return  connection;
    }

     @Bean
     public DefaultMessageListenerContainer messageListenerContainerJmsTestReceiver() {
     DefaultMessageListenerContainer messageListenerContainer = new DefaultMessageListenerContainer();
     messageListenerContainer.setConnectionFactory(cachingConnectionFactory());
     messageListenerContainer.setTaskExecutor(taskExecutorListener);
     messageListenerContainer.setDestination(activeMQQueueJMSTest());
     messageListenerContainer.setMessageListener(jmsMessageListener);
     messageListenerContainer.setCacheLevelName("CACHE_CONSUMER");
     return messageListenerContainer;
     }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public PooledConnectionFactory pooledConnectionFactory() {
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory());
        pooledConnectionFactory.setMaxConnections(jmsMaxConnections);
        pooledConnectionFactory.setMaximumActiveSessionPerConnection(jmsMaximumActiveSessionPerConnection);
        return pooledConnectionFactory;
    }



}
