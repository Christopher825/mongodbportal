package my.web.portal.config;



import my.web.portal.aspect.AuditAdvice;
import my.web.portal.aspect.SendToMessageAspect;
import my.web.portal.dao.audit.AuditDao;
import my.web.portal.dao.audit.AuditDaoImpl;
import my.web.portal.utils.ExceptionProcessor;
import org.aspectj.lang.Aspects;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "my.web.portal")
public class AspectConfig {

    @Bean
    public ExceptionProcessor exceptionProcessor(){
        return  new ExceptionProcessor();
    }


    @Bean
    public AuditDao auditDao() {
        return new AuditDaoImpl();
    }

    /*
     @create sendToMessageAspect bean with aspect factory method
     */
    @Bean
    public SendToMessageAspect sendToMessageAspect() {
        return Aspects.aspectOf(SendToMessageAspect.class);
    }

    /*
     @create auditAdvice bean with aspect factory method
     */
    @Bean
    public AuditAdvice auditAdvice(){
        return Aspects.aspectOf(AuditAdvice.class);
    }

}