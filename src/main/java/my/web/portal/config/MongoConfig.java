package my.web.portal.config;


import com.mongodb.*;
import my.web.portal.converters.QNameReadConverter;
import my.web.portal.converters.QNameWriteConverter;
import my.web.portal.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.*;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by christopherloganathan on 6/14/15.
 */

@Configuration
public class MongoConfig implements DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(MongoConfig.class);
    @Value("${dbname}")
    private String dbname;
    @Value("${url}")
    private String urls[];
    @Value("${port}")
    private int port;
    @Value("${connectionsPerHost}")
    private int connectionsPerHost;
    @Value("${threadsAllowedToBlockForConnectionMultiplier}")
    private int threadsAllowedToBlockForConnectionMultiplier;
    @Value("${connectTimeout}")
    private int connectTimeout;
    @Value("${socketKeepAlive}")
    private boolean socketKeepAlive;
    @Value("${socketTimeout}")
    private int socketTimeout;
    @Value("${write_number}")
    private  int write_number;
    @Value("${write_timeout}")
    private  int write_timeout;
    @Value("${username}")
    private String username;
    @Value("${password}")
    private String password;
    @Autowired
    private String environment;

    @Bean
    public MongoDbFactory mongoDbFactory() throws UnknownHostException {
        MongoClient mongo = null;
        List<ServerAddress> serverAddressList = new LinkedList<>();

        if (environment.equalsIgnoreCase("local")) {

            for(String url:urls){
                serverAddressList.add(new ServerAddress(url.trim(), port));
            }
            mongo = new MongoClient(serverAddressList, mongoClientOptions());
        } else if (environment.equalsIgnoreCase("dev")) {
            for(String url:urls){
                serverAddressList.add(new ServerAddress(url.trim(), port));
            }
            MongoCredential credential = MongoCredential.createCredential(
                    username,
                    dbname,
                    password.toCharArray());
            mongo = new MongoClient(serverAddressList, Arrays.asList(credential), mongoClientOptions());
        }
        logger.debug("Replica Set:::\n" + (CommonUtils.isEmpty(mongo) ? null : mongo.getReplicaSetStatus()));
        return new SimpleMongoDbFactory(mongo, dbname);
    }


    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate template = new MongoTemplate(mongoDbFactory(), mongoConverter());
        template.setWriteConcern(WriteConcern.NORMAL);
        template.setReadPreference(ReadPreference.secondaryPreferred());
        return template;
    }
    @Bean
    public MongoClientOptions mongoClientOptions(){

        MongoClientOptions mongoOptions =
                new MongoClientOptions.Builder()
                        .connectionsPerHost(connectionsPerHost)
                        .threadsAllowedToBlockForConnectionMultiplier(threadsAllowedToBlockForConnectionMultiplier)
                        .connectTimeout(connectTimeout)
                        .socketKeepAlive(socketKeepAlive)
                        .socketTimeout(socketTimeout)
                        .writeConcern(new WriteConcern(write_number, write_timeout)).build();
        return mongoOptions;
    }

    @Bean
    public CustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<Converter<?, ?>>();
        converters.add(new QNameReadConverter());
        converters.add(new QNameWriteConverter());
        return new CustomConversions(converters);
    }


    @Bean
    public MappingMongoConverter mongoConverter() throws Exception {
        MongoMappingContext mappingContext = new MongoMappingContext();
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
        MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, mappingContext);
        mongoConverter.setCustomConversions(customConversions());
        return mongoConverter;
    }

    @Override
    public void destroy() throws Exception {
        logger.debug("Shutdown Mongo DB connection");
        mongoTemplate().getDb().getMongo().close();
        logger.debug("Mongo DB connection shutdown completed");
    }
}
