package my.web.portal.config;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Order(1)
public class WebAppSecurityInitializer extends AbstractSecurityWebApplicationInitializer {


}