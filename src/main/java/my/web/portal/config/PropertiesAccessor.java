package my.web.portal.config;

import org.springframework.beans.factory.support.AbstractBeanFactory;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by christopherloganathan on 6/29/15.
 */
@Named(value = "urlProp")
public class PropertiesAccessor {

    private final AbstractBeanFactory beanFactory;

    private final Map<String, String> cache = new ConcurrentHashMap<>();

    @Inject
    protected PropertiesAccessor(AbstractBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public String getProperty(String key) {
        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        String foundProp = null;
        try {
            foundProp = beanFactory.resolveEmbeddedValue("${" + key.trim()
                    + "}");
            cache.put(key, foundProp);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }

        return foundProp;
    }

}
