package my.web.portal.config;


import my.web.portal.Application;
import my.web.portal.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;

import java.util.Properties;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Configuration
@ComponentScan(basePackageClasses = Application.class, excludeFilters = @ComponentScan.Filter({Controller.class, Configuration.class}))
public class MainConfig {


	@Value("${email.host}")
	private String emailHost;

	@Value("${email.auth}")
	private boolean emailAuth;

	@Value("${email.starttls.enable}")
	private boolean emailStarTlsEnable;

	@Value("${email.port}")
	private int emailPort;

	@Value("${email.username}")
	private String emailUsername;

	@Value("${email.password}")
	private String emailPassword;

	@Value("${threadCorePoolSize}")
	private int threadCorePoolSize;

	@Value("${threadMaxPoolSize}")
	private int threadMaxPoolSize;

	@Value("${waitForTasksToCompleteOnShutdown}")
	private boolean waitForTasksToCompleteOnShutdown;

	@Bean
	public ThreadPoolTaskExecutor taskExecutorListener() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(threadCorePoolSize);
		pool.setMaxPoolSize(threadMaxPoolSize);
		pool.setWaitForTasksToCompleteOnShutdown(waitForTasksToCompleteOnShutdown);
		return pool;
	}
	
	@Bean
	public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
		PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		ppc.setLocation(new ClassPathResource(getEnviroment().equals("dev") ? "./env/dev/portal.properties" : "./env/local/portal.properties"));
		return ppc;
	}

	@Bean
	public static String getEnviroment(){
		return CommonUtils.getEnv();
	}

	@Bean
	public JavaMailSenderImpl mailSenderImpl() {
		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		Properties props = new Properties();
		props.put("mail.smtp.auth", emailAuth);
		props.put("mail.smtp.starttls.enable", emailStarTlsEnable);
		props.put("mail.smtp.host", emailHost);
		props.put("mail.smtp.port", emailPort);
		javaMailSenderImpl.setJavaMailProperties(props);
		javaMailSenderImpl.setUsername(emailUsername);
		javaMailSenderImpl.setPassword(emailPassword);

		return javaMailSenderImpl;
	}





}