package my.web.portal.validator;

import my.web.portal.annotation.UsernameExistsNewAccountConstraint;
import my.web.portal.controller.AbstractController;
import my.web.portal.service.user.UserService;
import my.web.portal.utils.CommonUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by christopherloganathan on 7/10/15.
 */
public class UsernameExistsNewAccountConstraintValidator extends AbstractController implements ConstraintValidator<UsernameExistsNewAccountConstraint,Object> {

    private static final Logger logger = LoggerFactory.getLogger(UsernameExistsNewAccountConstraintValidator.class);

    @Autowired
    private UserService userService;

    private String username;

    @Override
    public void initialize(final UsernameExistsNewAccountConstraint usernameExistsNewAccountConstraint) {
        username = usernameExistsNewAccountConstraint.username();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        boolean toReturn =true;
        try {
            username = BeanUtils.getProperty(value, "username");

            if (CommonUtils.isEmpty(username)) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{notBlank.username}").addNode("username").addConstraintViolation();
                toReturn = false;

            }else if(!CommonUtils.isEmpty(userService.findUserByUsername(username))) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{username.notUnique}").addNode("username").addConstraintViolation();
                toReturn = false;

            }
        }catch(Exception ex){
            processException(ex);
        }
        return toReturn;
    }
}
