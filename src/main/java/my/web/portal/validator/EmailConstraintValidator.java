package my.web.portal.validator;

import my.web.portal.annotation.EmailPatternConstraint;
import my.web.portal.controller.AbstractController;
import my.web.portal.service.user.UserService;
import my.web.portal.utils.CommonUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by christopherloganathan on 6/24/15.
 */
public class EmailConstraintValidator  extends AbstractController implements ConstraintValidator<EmailPatternConstraint,Object> {

    private static final Logger logger = LoggerFactory.getLogger(EmailConstraintValidator.class);

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Autowired
    private UserService userService;

    private String email;
    private String userID;


    @Override
    public void initialize(final EmailPatternConstraint emailPatternConstraint) {
       email = emailPatternConstraint.email();
        userID = emailPatternConstraint.userID();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        boolean toReturn =true;
        try {
            email = BeanUtils.getProperty(value, "email");
            userID = BeanUtils.getProperty(value, "id");
            Pattern pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);

            if (CommonUtils.isEmpty(email)) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{notBlank.email}").addNode("email").addConstraintViolation();
                toReturn = false;

            }else if (!matcher.matches()) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{error.Email}").addNode("email").addConstraintViolation();
                toReturn = false;

            }else if(!CommonUtils.isEmpty(userID) && !CommonUtils.isEmpty(userService.findUserByEmail("email"))) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{error.email.exist}").addNode("email").addConstraintViolation();
                toReturn = false;

            }else if(!(email.equals(userService.findUserByID(userID).getEmail()) || CommonUtils.isEmpty(userService.findUserByEmail(email)))){
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{error.email.exist}").addNode("email").addConstraintViolation();
                toReturn = false;
            }

        }catch(Exception ex){
            processException(ex);
        }
        return toReturn;
    }
}

