package my.web.portal.validator;

import my.web.portal.annotation.UsernameExistsConstraint;
import my.web.portal.controller.AbstractController;
import my.web.portal.service.user.UserService;
import my.web.portal.utils.CommonUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by christopherloganathan on 6/24/15.
 */
public class UsernameExistsConstraintValidator extends AbstractController implements ConstraintValidator<UsernameExistsConstraint,Object> {

    private static final Logger logger = LoggerFactory.getLogger(UsernameExistsConstraintValidator.class);


    @Autowired
    private UserService userService;

    private String username;

    private String userID;


    @Override
    public void initialize(final UsernameExistsConstraint usernameExistsConstraint) {
        username = usernameExistsConstraint.username();
        userID= usernameExistsConstraint.userID();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        boolean toReturn =true;
        try {
            username = BeanUtils.getProperty(value, "username");
            userID = BeanUtils.getProperty(value, "id");

            if (CommonUtils.isEmpty(username)) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{notBlank.username}").addNode("username").addConstraintViolation();
                toReturn = false;

            }else if(CommonUtils.isEmpty(userID) && !CommonUtils.isEmpty(userService.findUserByUsername("username"))) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{username.notUnique}").addNode("username").addConstraintViolation();
                toReturn = false;

            }else if(!(username.equals(userService.findUserByID(userID).getUsername()) || CommonUtils.isEmpty(userService.findUserByUsername(username)))){
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{username.notUnique}").addNode("username").addConstraintViolation();
                toReturn = false;
            }

        }catch(Exception ex){
            processException(ex);
        }
        return toReturn;
    }

}
