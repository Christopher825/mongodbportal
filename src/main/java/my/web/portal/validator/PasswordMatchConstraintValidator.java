package my.web.portal.validator;

import my.web.portal.annotation.PasswordMatchConstraint;
import my.web.portal.controller.AbstractController;
import my.web.portal.utils.CommonUtils;
import my.web.portal.utils.PasswordStrategy;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by christopherloganathan on 6/26/15.
 */
public class PasswordMatchConstraintValidator extends AbstractController implements ConstraintValidator<PasswordMatchConstraint, Object> {

    private String password;
    private String secondpassword;

    private static final Logger logger = LoggerFactory.getLogger(PasswordMatchConstraintValidator.class);

    @Override
    public void initialize(final PasswordMatchConstraint constraintAnnotation)
    {
        password = constraintAnnotation.password();
        secondpassword = constraintAnnotation.confirmPassword();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        boolean toReturn = true;
        String firstPassword;
        String secondPassword;
        try {

            firstPassword = BeanUtils.getProperty(value, password);
            secondPassword = BeanUtils.getProperty(value, secondpassword);

            if(CommonUtils.isEmpty(firstPassword)){
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{notBlank.password}").addNode("password").addConstraintViolation();
                toReturn = false;
            }else if(!PasswordStrategy.STRONG_STRATEGY.validate(firstPassword)){
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{"+PasswordStrategy.STRONG_STRATEGY.getMessage()+"}").addNode("password").addConstraintViolation();
                toReturn = false;
            }else if(!firstPassword.equals(secondPassword)) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{password.confirm}").addNode("secondpassword").addConstraintViolation();
                toReturn = false;
            }
        } catch (Exception ex) {
            processException(ex);
        }

        return toReturn;
    }
}
