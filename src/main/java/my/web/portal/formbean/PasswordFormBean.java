package my.web.portal.formbean;

import lombok.Getter;
import lombok.Setter;
import my.web.portal.annotation.PasswordMatchConstraint;

/**
 * Created by christopherloganathan on 6/22/15.
 */

@PasswordMatchConstraint(password = "password",confirmPassword = "secondpassword")
public class PasswordFormBean {


    @Getter @Setter private String password;

    @Getter @Setter private String secondpassword;
}
