package my.web.portal.formbean;

import lombok.Getter;
import lombok.Setter;
import my.web.portal.annotation.EmailPatternConstraint;
import my.web.portal.annotation.UsernameExistsConstraint;
import org.hibernate.validator.constraints.NotEmpty;

@EmailPatternConstraint(email = "email",userID = "id")
@UsernameExistsConstraint(username = "username",userID = "id")
public class AccountFormBean {

	@Getter @Setter private String id;

	@Getter @Setter private String username;

	@NotEmpty(message = "{notBlank.firstname}")
	@Getter @Setter private String firstname;

	@NotEmpty(message = "{notBlank.lastname}")
	@Getter @Setter private String lastname;

	@Getter @Setter private String email;

	@NotEmpty(message = "{notBlank.role}")
	@Getter @Setter private String role;

}
