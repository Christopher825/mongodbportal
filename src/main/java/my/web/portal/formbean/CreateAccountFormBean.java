package my.web.portal.formbean;

import lombok.Getter;
import lombok.Setter;
import my.web.portal.annotation.EmailPatternNewAccountConstraint;
import my.web.portal.annotation.UsernameExistsNewAccountConstraint;
import my.web.portal.model.user.User;
import my.web.portal.utils.AccountStatus;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by christopherloganathan on 6/27/15.
 */
@EmailPatternNewAccountConstraint(email = "email")
@UsernameExistsNewAccountConstraint(username = "username")
public class CreateAccountFormBean {

    @Getter
    @Setter
    private String username;

    @NotEmpty(message = "{notBlank.firstname}")
    @Getter @Setter private String firstname;

    @NotEmpty(message = "{notBlank.lastname}")
    @Getter @Setter private String lastname;

    @Getter @Setter private String email;

    @NotEmpty(message = "{notBlank.role}")
    @Getter @Setter private String role;

    public User createAccount() {
        return new User(getUsername(), getRole(), AccountStatus.ACTIVE.getStatus());
    }
}
