package my.web.portal.converters;

import com.mongodb.DBObject;
import org.springframework.core.convert.converter.Converter;
import javax.xml.namespace.QName;

/**
 * Created by christopherloganathan on 8/5/15.
 *
 * @handle objects with mapping e.g. objects without default constructor
 */
public final class QNameReadConverter implements Converter<DBObject, QName> {

    public QName convert(DBObject source) {
        return new QName((String) source.get("namespaceURI"), (String) source.get("localPart"), (String) source.get("prefix"));

    }
}
