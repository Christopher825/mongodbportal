package my.web.portal.converters;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.core.convert.converter.Converter;
import javax.xml.namespace.QName;

/**
 * Created by christopherloganathan on 8/5/15.
 *
 * @handle objects with mapping e.g. objects without default constructor
 */
public final class QNameWriteConverter implements Converter<QName, DBObject> {

    public DBObject convert(QName source) {


        DBObject dbo = new BasicDBObject();
        dbo.put("namespaceURI", source.getNamespaceURI());
        dbo.put("localPart", source.getLocalPart());
        dbo.put("prefix", source.getPrefix());
        return dbo;
    }
}