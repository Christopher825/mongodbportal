package my.web.portal.handlers;


import my.web.portal.annotation.AuditTrail;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by christopherloganathan on 7/11/15.
 */
public final class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @AuditTrail("Failed to login")
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        if (BadCredentialsException.class.isInstance(e)){
            response.sendRedirect(request.getContextPath() + "/login?error1=1");
        }else if(LockedException.class.isInstance(e)){
            response.sendRedirect(request.getContextPath() + "/login?error2=2");
        }
    }
}
