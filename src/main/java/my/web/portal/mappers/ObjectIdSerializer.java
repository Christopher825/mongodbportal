package my.web.portal.mappers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.bson.types.ObjectId;
import java.io.IOException;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public final class ObjectIdSerializer extends JsonSerializer<ObjectId> {

    @Override
    public void serialize(ObjectId value, JsonGenerator jgen, SerializerProvider provider) throws IOException {

        if (value == null) {
            jgen.writeNull();
        } else {
            jgen.writeString(value.toString());
        }
    }
}
