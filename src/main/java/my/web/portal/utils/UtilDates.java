package my.web.portal.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by christopherloganathan on 6/24/15.
 */
public class UtilDates {

    public static final DateFormat auditDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

    public static Date currentDateTime() {
        Date current = new Date();
        Calendar newDate = Calendar.getInstance();
        newDate.setTime(current);
        return newDate.getTime();
    }

    public static Date toDateTime (String  param,DateFormat dateFormat){
        Calendar newDate = Calendar.getInstance();
        try {
            newDate.setTime(CommonUtils.isEmpty(param)?new Date():dateFormat.parse(param));
        }catch (ParseException pex){
            pex.getStackTrace();
            newDate.setTime(new Date());
        }
        return newDate.getTime();

    }

    public static Calendar currentCalendarDateTime() {
        return Calendar.getInstance();
    }

}
