package my.web.portal.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Created by christopherloganathan on 6/28/15.
 */
@Service
public class EmailService {


    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine templateEngine;

    public void sendUserFirstTimeRegistrationEmail(
            HttpServletRequest request,
            HttpServletResponse response,
            String recipientFrom,
            String recipientTo,
            String subject,
            Map<String,String> var) throws MessagingException {

        HttpSession session = request.getSession();

        WebContext ctx = new WebContext(request,response,session.getServletContext(),request.getLocale());
        ctx.setVariables(var);

        MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
        message.setSubject(subject);
        message.setFrom(recipientFrom);
        message.setTo(recipientTo);

        String htmlContent = this.templateEngine.process("email/first_time_user_invite", ctx);
        message.setText(htmlContent, true /* isHtml */);

        this.mailSender.send(mimeMessage);

    }
}
