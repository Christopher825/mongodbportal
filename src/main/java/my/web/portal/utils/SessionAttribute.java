package my.web.portal.utils;

import lombok.Getter;

/**
 * Created by christopherloganathan on 6/17/15.
 */
public enum SessionAttribute {

    ID("userID"),
    LOCK("lock"),
    USERNAME("username"),
    SEARCH_ACCOUNT("searchAccount"),
    START_DATE_AUDIT("startDateAudit"),
    END_DATE_AUDIT("endDateAudit"),
    PAGE_VIEW_SEARCH_ACCOUNT("pageViewSearchAccount"),
    PAGE_VIEW_START_DATE_AUDIT("pageViewStartDateAudit"),
    PAGE_VIEW_END_DATE_AUDIT("pageViewEndDateAudit");
    @Getter
    private String attribute;

    SessionAttribute(String attribute) {
        this.attribute = attribute;
    }
}
