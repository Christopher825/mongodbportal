package my.web.portal.utils;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public class AccessRoles {

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";


}
