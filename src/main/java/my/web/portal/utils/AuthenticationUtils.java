package my.web.portal.utils;

import my.web.portal.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Component
public class AuthenticationUtils {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationUtils.class);

    private static AuthenticationManager authenticationManager;


    @Autowired(required=true)
    private AuthenticationUtils(AuthenticationManager authenticationManager){
        AuthenticationUtils.authenticationManager = authenticationManager;
    }

    public static User getAuthenticatedUser(){
        User user = null;
        if (SecurityContextHolder.getContext()
                .getAuthentication() != null
                && SecurityContextHolder.getContext()
                .getAuthentication().isAuthenticated() && SecurityContextHolder
                .getContext().getAuthentication().getPrincipal() instanceof User)

            user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();


        return user;
    }

    public static void autoLogin(String username, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                username, password);

        Authentication auth = AuthenticationUtils.authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(auth);
        logger.info("+++++Auto login Sucessfull++++++");
    }

}
