package my.web.portal.utils;

/**
 * Created by christopherloganathan on 6/15/15.
 */
public class CustomMessageIndicator {

	static final String MESSAGE_ATTRIBUTE = "message";


	public enum Type {
        DANGER, WARNING, INFO, SUCCESS;
	}

	private final String message;
	private final Type type;
	private final Object[] args;

	public CustomMessageIndicator(String message, Type type) {
		this.message = message;
		this.type = type;
		this.args = null;
	}
	
	public CustomMessageIndicator(String message, Type type, Object... args) {
		this.message = message;
		this.type = type;
		this.args = args;
	}

	public String getMessage() {
		return message;
	}

	public Type getType() {
		return type;
	}

	public Object[] getArgs() {
		return args;
	}
}
