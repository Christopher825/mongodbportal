package my.web.portal.utils;


import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;

/**
 * Created by christopherloganathan on 6/25/15.
 */
public class Token {

    private static final Logger logger = LoggerFactory.getLogger(Token.class);
    private static final String encryptionKey = "#$@@@#$%%^^^#@@@@";
    private static SecretKeySpec secretKey;


    static {

        MessageDigest sha = null;
        try {

            byte[] key = encryptionKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public static String encrypt(String strToEncrypt){
        try{
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.encodeBase64URLSafeString(cipher.doFinal(strToEncrypt
                    .getBytes("UTF-8")));
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "";
    }

    public static String decrypt(String strToDecrypt){
        try{
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "";
    }

    public static String generateToken(String username) {
        return RandomStringUtils.randomAlphanumeric(10) + "|" + username + "|"+System.currentTimeMillis();
    }



    public static boolean checkAccessTokenValidation(String token,int expire) {

        String accToken[] = token.split("[|]");
        long diffTime = (System.currentTimeMillis() - Long
                .parseLong(accToken[2].trim())) / 1000 / 60;
        logger.debug("Time Differ::" + diffTime);
        if (diffTime > expire)
            return true;
        else
            return false;

    }

    public static Date getNewValidationExpireDate(String token,int expire) {
        String accToken[] = token.split("[|]");
        Date preDate = new Date(Long.parseLong(accToken[2].trim()));
        Calendar newDate = UtilDates.currentCalendarDateTime();
        newDate.setTime(preDate);
        newDate.add(Calendar.MINUTE, expire);
        logger.debug("Date time new Validation:::" + newDate.getTime());
        return newDate.getTime();

    }

}
