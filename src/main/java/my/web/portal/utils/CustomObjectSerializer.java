package my.web.portal.utils;


import my.web.portal.mappers.CustomObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by christopherloganathan on 6/14/15.
 * #handle:Use custom object if need to serializer collection object to avoid truncated objectID bson serialize to json object
 */
public class CustomObjectSerializer {

    public static final Object serializerObjectID(Object obj) throws Exception {

        //serialize data with objectID
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CustomObjectMapper mapper = new CustomObjectMapper();
        mapper.writeValue(baos, obj);

        //deserialize data with objectID
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        return mapper.readValue(bais, Object.class);
    }
}
