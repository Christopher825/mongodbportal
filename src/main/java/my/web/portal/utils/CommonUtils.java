package my.web.portal.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.List;


/**
 * Created by christopherloganathan on 6/15/15.
 */
public class CommonUtils {

    public static final String pageParamName="page.page";
    public static final String pageParamSize="page.size";
    public static final int MAX_PAGE_ITEM_DISPLAY = 5;
    private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    // To get remote address if client ip goes through proxy servers.
    public static final String getRemoteAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }

    public static final String getEnv(){
        java.net.InetAddress addr = null;
        try {
            addr = java.net.InetAddress.getLocalHost();
            if(!isEmpty(addr) && addr.getHostName().toUpperCase().indexOf("DEV") > -1){
                logger.info("Hostname Dev:::" + addr.getHostName());
                return "dev";
            }else{
                logger.info("Hostname Local :::" + addr.getHostName());
               return "local";
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return"local";
        }

    }

    public static final String getHashPassword(String password){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11);
        return passwordEncoder.encode(password);
    }

    public static final boolean isEmpty(Object object) {
        if (object == null || object.toString().trim().equals("")) {
            return true;
        }

        return false;
    }

    //List
    public static final boolean isEmpty(List object) {
        if (object == null || object.size() == 0) {
            return true;
        }

        return false;
    }

    public static final String createGsonBuilder(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }


    public static final String getDefaultStringValue(String str, String defaultStr) {
        return str == null ? defaultStr : str;
    }

}
