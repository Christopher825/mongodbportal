package my.web.portal.utils;

import lombok.Getter;

/**
 * Created by christopherloganathan on 6/24/15.
 */
public enum AccountStatus {

    ACTIVE("active"), LOCK("locked"),UNLOCK("unlocked"),INACTIVE("inactive"),PENDING("pending");
    @Getter
    private String status;

    AccountStatus(String status) {
        this.status = status;
    }
}
