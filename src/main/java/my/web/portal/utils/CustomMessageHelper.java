package my.web.portal.utils;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by christopherloganathan on 6/15/15.
 */
public final class CustomMessageHelper {

    private CustomMessageHelper() {

    }

    public static void addSuccessAttribute(RedirectAttributes ra, String message, Object... args) {
        addAttribute(ra, message, CustomMessageIndicator.Type.SUCCESS, args);
    }

    public static void addErrorAttribute(RedirectAttributes ra, String message, Object... args) {
        addAttribute(ra, message, CustomMessageIndicator.Type.DANGER, args);
    }

    public static void addInfoAttribute(RedirectAttributes ra, String message, Object... args) {
        addAttribute(ra, message, CustomMessageIndicator.Type.INFO, args);
    }

    public static void addWarningAttribute(RedirectAttributes ra, String message, Object... args) {
        addAttribute(ra, message, CustomMessageIndicator.Type.WARNING, args);
    }

    private static void addAttribute(RedirectAttributes ra, String message, CustomMessageIndicator.Type type, Object... args) {
        ra.addFlashAttribute(CustomMessageIndicator.MESSAGE_ATTRIBUTE, new CustomMessageIndicator(message, type, args));
    }

    public static void addSuccessAttribute(Model model, String message, Object... args) {
        addAttribute(model, message, CustomMessageIndicator.Type.SUCCESS, args);
    }

    public static void addErrorAttribute(Model model, String message, Object... args) {
        addAttribute(model, message, CustomMessageIndicator.Type.DANGER, args);
    }

    public static void addInfoAttribute(Model model, String message, Object... args) {
        addAttribute(model, message, CustomMessageIndicator.Type.INFO, args);
    }

    public static void addWarningAttribute(Model model, String message, Object... args) {
        addAttribute(model, message, CustomMessageIndicator.Type.WARNING, args);
    }

    private static void addAttribute(Model model, String message, CustomMessageIndicator.Type type, Object... args) {
        model.addAttribute(CustomMessageIndicator.MESSAGE_ATTRIBUTE, new CustomMessageIndicator(message, type, args));
    }
}
