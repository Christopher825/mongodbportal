package my.web.portal.utils;

import lombok.Getter;

/**
 * Created by christopherloganathan on 6/23/15.
 */
public enum Role {

    ADMIN("admin"), USER("user"),ANONYMOUS("anonymous");
    @Getter private String role;

    Role(String role) {
        this.role = role;
    }




}
