package my.web.portal.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by christopherloganathan on 6/27/15.
 */
public enum PasswordStrategy {

    NO_STRATEGY(".*",0,"error.invalid_password.NO_STRATEGY"),
    DIGIT_STRATEGY("\\d[0-9]",0,"error.invalid_password.DIGIT_STRATEGY"),
    STRONG_STRATEGY("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d.*\\d)[a-zA-Z0-9.!@#$%^&*()\\s]{6,30}$",0,"error.invalid_password.STRONG_STRATEGY"),
    PASSPHRASE_STRATEGY("^(?=.*[a-z])(?=.*[A-Z])(?=.*[\\s])[a-zA-Z0-9\\s]{6,30}$",0,"error.invalid_password.PASSPHRASE_STRATEGY"),
    USERID_STRATEGY("^[a-zA-Z0-9.!@#$%^&*()\\s]{6,30}$",0, "error.invalid_userid.USERID_STRATEGY");



    PasswordStrategy(String pattern,int flags,String message){
        this.pattern = pattern;
        this.message = message;
        this.flags = flags;
    }

    private final String pattern;
    private final String message;
    private final int flags;
    private Pattern patternRegEx;

    public String getMessage(){

        return message;

    }

    public boolean validate(Object value) {
        patternRegEx = Pattern.compile(
                pattern,
                flags );

        if ( value == null ) return true;
        if ( !( value instanceof String ) ) return false;
        String password = (String) value;
        Matcher m = patternRegEx.matcher( password );
        return m.matches();
    }
}
