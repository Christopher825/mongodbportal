package my.web.portal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Date;

/**
 * Created by christopherloganathan on 6/17/15.
 */
public final class SessionListener implements HttpSessionListener {

    private static final Logger logger = LoggerFactory.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        logger.info("Session ID [" + event.getSession().getId() + "] created at " + new Date());
        if(event.getSession().isNew()) {
            event.getSession().setMaxInactiveInterval(5 * 60);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        logger.info("Session ID [" + event.getSession().getId() + "] destroyed at " + new Date());
    }
}

