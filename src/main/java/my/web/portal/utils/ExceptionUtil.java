package my.web.portal.utils;

import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by christopherloganathan on 6/16/15.
 */
public class ExceptionUtil {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionUtil.class);

    public static void handleAllCaughtException(Exception exception) {

        Throwable rootCause = Throwables.getRootCause(exception);
        logger.error(rootCause.toString(), exception);
    }
}
