package my.web.portal.service.audit;

import my.web.portal.dao.audit.AuditDao;
import my.web.portal.model.audit.Audit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by christopherloganathan on 15/03/2016.
 */
@Service
public final class AuditServiceImpl implements AuditService {

    @Autowired
    private AuditDao auditDao;

    @Override
    public void create(Audit audit){
        auditDao.create(audit);
    }

    @Override
    public List<Audit> list(){
        return auditDao.list();
    }

    @Override
    public Page<Audit> list(Pageable pageable,String startDate,String endDate){
        return auditDao.list(pageable,startDate,endDate);
    }
}
