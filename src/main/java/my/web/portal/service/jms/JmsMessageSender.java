package my.web.portal.service.jms;

import my.web.portal.config.PropertiesAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.MessageCreator;

import java.io.Serializable;


/**
 * Created by christopherloganathan on 7/14/15.
 * @handle :JMS sender
 */
@Service
public class JmsMessageSender {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private PropertiesAccessor propertiesAccessor;

    public void sendObjectMessage(final Destination dest, final Serializable obj) {

        this.jmsTemplate.send(dest, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = session.createObjectMessage(obj);
                return message;
            }
        });
    }


    public void send(final String text) {

        this.jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = session.createTextMessage(text);
                message.setJMSReplyTo(new ActiveMQQueue(propertiesAccessor.getProperty("jms.test.ReplyToQ")));
                return message;
            }
        });
    }


    public void convertAndSend(final String text) {
        this.jmsTemplate.convertAndSend(text);
    }

    public void send(final Destination dest,final String text) {

        this.jmsTemplate.send(dest,new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = session.createTextMessage(text);
                return message;
            }
        });
    }

    public void convertAndSend(final String destination, final String text) {
        this.jmsTemplate.convertAndSend(destination, text);
    }


    public void send(final String dest, final String text) {

        this.jmsTemplate.send(dest, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = session.createTextMessage(text);
                return message;
            }
        });
    }
}
