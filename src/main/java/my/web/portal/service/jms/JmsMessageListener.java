package my.web.portal.service.jms;

import my.web.portal.controller.AbstractController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * Created by christopherloganathan on 8/23/15.
 */
@Service
public class JmsMessageListener extends AbstractController implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(JmsMessageListener.class);
    @Override
    public void onMessage(Message message) {
        logger.info("Received: " + message);
    }
}
