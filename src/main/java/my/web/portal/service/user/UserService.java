package my.web.portal.service.user;

import my.web.portal.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public interface UserService {

    void create(User user);

    void delete(User user);

    void update(User user);

    List<User> list();

    User findUserByUsername(String username);


    User findUserByID(String id);

    User findUserByEmail(String email);

    Page<User> list(Pageable pageable,String excludeAdminUserLogin,String search);


}
