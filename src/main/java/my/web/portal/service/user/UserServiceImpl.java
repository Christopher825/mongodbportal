package my.web.portal.service.user;

import my.web.portal.controller.AbstractController;
import my.web.portal.dao.user.UserDao;
import my.web.portal.model.user.User;
import my.web.portal.model.user.UserName;
import my.web.portal.model.user.UserNameAudit;
import my.web.portal.utils.AccessRoles;
import my.web.portal.utils.AccountStatus;
import my.web.portal.utils.CommonUtils;
import my.web.portal.utils.UtilDates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Service
public final class UserServiceImpl extends AbstractController implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public void create(User user) {
        userDao.create(user);
    }

    @Override
    public void delete(User user) {
        userDao.delete(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public List<User> list() {
        return userDao.list();
    }

    @Override
    public User findUserByUsername(String username) {
        return userDao.findUserByUsername(username);
    }

    @Override
    public User findUserByID(String id) {
        return userDao.findUserByID(id);
    }

    @Override
    public User findUserByEmail(String email) {
        return userDao.findUserByEmail(email);
    }

    @Override
    public Page<User> list(Pageable pageable,String excludeAdminUserLogin,String search) {
        return userDao.list(pageable,excludeAdminUserLogin,search);
    }

    @PostConstruct
    protected void initialize() {
        if(userDao.list().isEmpty()) {
            Date now = UtilDates.currentDateTime();
            User user = new User(urlProp.getProperty("adminUsername"), AccessRoles.ROLE_ADMIN, AccountStatus.ACTIVE.getStatus());
            user.setPassword(CommonUtils.getHashPassword(urlProp.getProperty("adminPassword")));
            user.setName(new UserName("", ""));
            user.setCreatedOn(now);
            user.setUserNameAudits(Arrays.asList(new UserNameAudit(user.getUsername(),now)));
            userDao.create(user);
        }
    }
}
