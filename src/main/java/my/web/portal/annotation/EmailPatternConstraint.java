package my.web.portal.annotation;

/**
 * Created by christopherloganathan on 6/24/15.
 */

import my.web.portal.validator.EmailConstraintValidator;
import javax.validation.Constraint;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Target;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy=EmailConstraintValidator.class)
public @interface EmailPatternConstraint {
    String message() default "";
    Class[] groups() default {};
    Class[] payload() default {};
    String email();
    String userID();
}
