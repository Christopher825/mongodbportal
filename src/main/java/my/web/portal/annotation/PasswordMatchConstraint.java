package my.web.portal.annotation;


import my.web.portal.validator.PasswordMatchConstraintValidator;
import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;
import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * Created by christopherloganathan on 6/26/15.
 */


@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy=PasswordMatchConstraintValidator.class)
public @interface PasswordMatchConstraint {

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String password();

    String confirmPassword();
}


