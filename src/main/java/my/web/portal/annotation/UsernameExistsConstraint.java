package my.web.portal.annotation;

import my.web.portal.validator.UsernameExistsConstraintValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Target;



/**
 * Created by christopherloganathan on 6/24/15.
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy=UsernameExistsConstraintValidator.class)
public @interface UsernameExistsConstraint {
    String message() default "";
    Class[] groups() default {};
    Class[] payload() default {};
    String username();
    String userID();
}
