package my.web.portal.annotation;


import my.web.portal.validator.UsernameExistsNewAccountConstraintValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by christopherloganathan on 6/24/15.
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy=UsernameExistsNewAccountConstraintValidator.class)
public @interface UsernameExistsNewAccountConstraint {
    String message() default "";
    Class[] groups() default {};
    Class[] payload() default {};
    String username();
}
