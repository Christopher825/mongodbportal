package my.web.portal.annotation;

import my.web.portal.validator.EmailConstraintNewAccountValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by christopherloganathan on 7/10/15.
 */

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy=EmailConstraintNewAccountValidator.class)
public @interface EmailPatternNewAccountConstraint {
    String message() default "";

    Class[] groups() default {};

    Class[] payload() default {};

    String email();

}