package my.web.portal.filters;

import my.web.portal.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public final class CorsFilter extends OncePerRequestFilter {

    private static final String ORIGIN = "Origin";
    private static final Logger logger = LoggerFactory.getLogger(CorsFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        RequestCallFilter.HttpMethodRequestWrapper httpRequest = new RequestCallFilter.HttpMethodRequestWrapper(request.getMethod(), request);
        logger.info("::::::::::::::::::STARTING CORS FILTER:::::::::::::::::::::::::");
        if (!CommonUtils.isEmpty(httpRequest.getHeader(ORIGIN))) {
            response.addHeader("Access-Control-Allow-Origin", "ws*,wss*");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.addHeader("Access-Control-Max-Age", "5");//allow default max 5 seconds response payload caching
            String reqHead = httpRequest.getHeader("Access-Control-Request-Headers");

            if (!CommonUtils.isEmpty(reqHead)) {
                response.addHeader("Access-Control-Allow-Headers", reqHead);
            }
        }
        if (httpRequest.getMethod().equals("OPTIONS")) {
            logger.debug("::::::::::::::::::OPTIONS REQUEST METHOD["+httpRequest.getMethod()+"]");
        }
        else {
            logger.debug("::::::::::::::::::NON OPTIONS REQUEST METHOD["+httpRequest.getMethod()+"]");
            filterChain.doFilter(httpRequest, response);
        }

    }
}

