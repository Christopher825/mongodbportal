package my.web.portal.filters;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public final class HttpResponse extends HttpServletResponseWrapper {
    private int httpStatusCode;

    public HttpResponse(HttpServletResponse response) {
        super(response);
    }

    @Override
    public void sendError(int sc) throws IOException {
        httpStatusCode = sc;
        super.sendError(sc);
    }

    @Override
    public void sendError(int sc, String msg) throws IOException {
        httpStatusCode = sc;
        super.sendError(sc, msg);
    }

    public int getStatus() {
        return httpStatusCode;
    }

    @Override
    public void setStatus(int sc) {
        httpStatusCode = sc;
        super.setStatus(sc);
    }
}

