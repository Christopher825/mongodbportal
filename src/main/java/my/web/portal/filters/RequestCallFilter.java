package my.web.portal.filters;

import my.web.portal.model.user.User;
import my.web.portal.utils.AuthenticationUtils;
import my.web.portal.utils.CommonUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public final class RequestCallFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(RequestCallFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String username = "Anonymous";
        HttpMethodRequestWrapper req = new HttpMethodRequestWrapper(request.getMethod(), request);

        User user = AuthenticationUtils.getAuthenticatedUser();
        if(user!=null)
            username = user.getUsername();


        if (!"HEAD".equals(req.getMethod()))
        {

            logger.info("method="+req.getMethod()+ ",rqurl=" +req.getRequestURL() + ",rmAdd=" + CommonUtils.getRemoteAddress(req) + ",username=" + username + ",jID=" + req.getRequestedSessionId() + ",rQ=" + req.getQueryString());
            filterChain.doFilter(req, response);
        }
        else
            filterChain.doFilter(new HttpMethodRequestWrapper("GET", request), response);
    }

     public static class HttpMethodRequestWrapper extends HttpServletRequestWrapper {

        private static final Logger logger = LoggerFactory.getLogger(HttpMethodRequestWrapper.class);
        private final String method;
        private Map<String, String> headerMap = new HashMap<>();

        public HttpMethodRequestWrapper(String method, HttpServletRequest request) {
            super(request);
            this.method = method;
        }

        public void addHeader(String name, String value) {
            headerMap.put(name, value);
        }

        public String getHeader(String name) {
            Object value;
            if ((value = headerMap.get(name)) != null) {
                String tempValue = StringEscapeUtils.escapeSql(value.toString());
                if (!CommonUtils.isEmpty(tempValue)) {
                    tempValue = Normalizer.normalize(tempValue, Normalizer.Form.NFD);
                    tempValue = tempValue.replaceAll("\0", "");
                }
                return tempValue;
            } else {
                String tempValue = StringEscapeUtils.escapeSql(((HttpServletRequest) getRequest()).getHeader(name));
                if (!CommonUtils.isEmpty(tempValue)) {
                    tempValue = Normalizer.normalize(tempValue, Normalizer.Form.NFD);
                    tempValue = tempValue.replaceAll("\0", "");
                }
                return tempValue;
            }

        }

        public String[] getParameterValues(String parameter) {

            String[] values = super.getParameterValues(parameter);
            if (values == null) {
                return null;
            }
            int count = values.length;
            String[] encodedValues = new String[count];
            for (int i = 0; i < count; i++) {
                String tempValue = StringEscapeUtils.escapeSql(values[i]);
                if (!CommonUtils.isEmpty(tempValue)) {
                    tempValue = Normalizer.normalize(tempValue, Normalizer.Form.NFD);
                    tempValue = tempValue.replaceAll("\0", "");
                }
                encodedValues[i] = tempValue;
            }

            return encodedValues;
        }

        public String getParameter(String parameter) {

            String value = super.getParameter(parameter);
            if (value == null) {
                return null;
            }
            String tempValue = StringEscapeUtils.escapeSql(value);
            if (!CommonUtils.isEmpty(tempValue)) {
                tempValue = Normalizer.normalize(tempValue, Normalizer.Form.NFD);
                tempValue = tempValue.replaceAll("\0", "");
            }
            return tempValue;
        }

        @Override
        public String getMethod() {
            return this.method;
        }
    }
}
