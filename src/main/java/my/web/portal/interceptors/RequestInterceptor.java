package my.web.portal.interceptors;

import my.web.portal.filters.RequestCallFilter;
import my.web.portal.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public final class RequestInterceptor extends HandlerInterceptorAdapter {

    private static final List<Pattern> patterns;
    private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

    static {
        patterns = Arrays.asList(
                Pattern.compile(".*?<script.*?>.*?</script.*?>.*?"),
                Pattern.compile(".*?<.*?\\s+javascript:.*?>.*?</.*?>.*?"),
                Pattern.compile(".*?<.*?\\s+on.*?>.*?</.*?>.*?")
        );

    }

    /*
     @inclusive with escape sql
     @handle malicious scripts and redirect to custom 400 page with message
     */

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler)
            throws Exception {

        logger.debug("::::::::::::::::Request XSS Pre-Handle:::::::::::::::");
        RequestCallFilter.HttpMethodRequestWrapper req = new RequestCallFilter.HttpMethodRequestWrapper(request.getMethod(), request);
        Enumeration enums = req.getParameterNames();
        while (enums.hasMoreElements()) {
            String parameterName = (String) enums.nextElement();
            if (filterXSS(req.getParameter(parameterName)) == false) {
                if (CommonUtils.isEmpty(req.getHeader("X-Requested-With")))
                    response.sendRedirect(req.getContextPath() + "/badRequest");
                else
                    response.sendError(400);

                return false;
            }
        }

        return true;

    }

    private boolean filterXSS(String value) {

        if (value != null) {
            if (!CommonUtils.isEmpty(patterns)) {
                for (Pattern pattern : patterns) {
                    if (pattern.matcher(value).matches()) {
                        return false;
                    }
                }
            }
        }
        return true;

    }


}