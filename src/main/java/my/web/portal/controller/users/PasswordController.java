package my.web.portal.controller.users;

import my.web.portal.annotation.AuditTrail;
import my.web.portal.controller.AbstractController;
import my.web.portal.formbean.PasswordFormBean;
import my.web.portal.model.user.User;
import my.web.portal.service.user.UserService;
import my.web.portal.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by christopherloganathan on 6/25/15.
 */
@Controller
public class PasswordController extends AbstractController {

    private static final String PASSWORD_VIEW_NAME = "account/password";
    private static final String PASSWORD_VIEW_NAME_FIRST_TIME = "account/passwordFirstTime";
    private static final Logger logger = LoggerFactory.getLogger(PasswordController.class);

    @Autowired
    private UserService userService;

    @Value("${password.expire}")
    private String passwordExp;

    @RequestMapping(value = "changePassword")
    public String changePasswordView(Model model) {
        model.addAttribute(new PasswordFormBean());
        return PASSWORD_VIEW_NAME;
    }

    @AuditTrail("Edit Password")
    @RequestMapping(value = "editPassword", method = RequestMethod.POST)
    public String editPassword(@Valid @ModelAttribute PasswordFormBean passwordFormBean, Errors errors, RedirectAttributes ra,Principal principal){

        if (errors.hasErrors()) {
            return PASSWORD_VIEW_NAME;
        }
        Date now = UtilDates.currentDateTime();
        User user = AuthenticationUtils.getAuthenticatedUser();
        user.setPassword(CommonUtils.getHashPassword(passwordFormBean.getPassword()));
        user.setUpdatedOn(now);
        userService.update(user);
        CustomMessageHelper.addSuccessAttribute(ra, "edit.password.success",principal.getName());
        return "redirect:/";
    }

    @AuditTrail("Edit First Time Password")
    @RequestMapping(value = "editPasswordFirstTime", method = RequestMethod.POST)
    public String editPasswordFirstTme(@Valid @ModelAttribute PasswordFormBean passwordFormBean, Errors errors, RedirectAttributes ra,HttpSession session){

        if (errors.hasErrors()) {
            return PASSWORD_VIEW_NAME_FIRST_TIME;
        }

        if(CommonUtils.isEmpty(session.getAttribute(SessionAttribute.ID.getAttribute()))){
            return "redirect:/";
        }else{
            Date now = UtilDates.currentDateTime();
            if(CommonUtils.isEmpty(session.getAttribute(SessionAttribute.ID.getAttribute()))){
                return "redirect:/";
            }else {
                User user = userService.findUserByID((String)session.getAttribute(SessionAttribute.ID.getAttribute()));
                user.setPassword(CommonUtils.getHashPassword(passwordFormBean.getPassword()));
                user.setExpireToken(null);
                user.setToken(null);
                user.setUpdatedOn(now);
                user.setStatus("active");
                userService.update(user);
                AuthenticationUtils.autoLogin(user.getUsername(), passwordFormBean.getPassword());
                CustomMessageHelper.addSuccessAttribute(ra, "edit.password.success", user.getUsername());
                session.removeAttribute(SessionAttribute.ID.getAttribute());
                return "redirect:/";
            }
        }

    }

    @AuditTrail("First Time Registration Account")
    @RequestMapping(value = "/show/firstTimeRegistration/{token}")
    public String editFirstimePassword(Model model,@PathVariable String token,HttpSession session){

        // To check validity of password link
        boolean linkValidity = true;
        String localToken = Token.decrypt(token);
        String accessToken[] = localToken.split("[|]");
        logger.info("Token:::::" + Arrays.toString(accessToken));
        User user = userService.findUserByUsername(accessToken[1]);
        if (!CommonUtils.isEmpty(user)&& !CommonUtils.isEmpty(user.getToken())) {
            logger.info("Request token::" + user.getToken());
            if (!user.getToken().equals(token) || Token.checkAccessTokenValidation(localToken,Integer.valueOf(getConfigProperty("password.expire"))))
                linkValidity = false;

        } else
            linkValidity = false;

        if (!linkValidity)
            return "redirect:/";
        else {
            session.setAttribute(SessionAttribute.ID.getAttribute(),user.getId().toString());
            model.addAttribute(new PasswordFormBean());
            return PASSWORD_VIEW_NAME_FIRST_TIME;
        }
    }


}
