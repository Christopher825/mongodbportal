package my.web.portal.controller.users;

import my.web.portal.annotation.AuditTrail;
import my.web.portal.controller.AbstractController;
import my.web.portal.formbean.CreateAccountFormBean;
import my.web.portal.model.user.TokenAudit;
import my.web.portal.model.user.UserName;
import my.web.portal.model.user.UserNameAudit;
import my.web.portal.service.user.UserService;
import my.web.portal.model.user.User;
import my.web.portal.utils.AccountStatus;
import my.web.portal.utils.CustomMessageHelper;
import my.web.portal.utils.Token;
import my.web.portal.utils.UtilDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.*;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Controller
public class CreateAccountController extends AbstractController {

    private static final String CREATE_ACCOUNT_VIEW_NAME = "account/createAccount";
	private static final Logger logger = LoggerFactory.getLogger(CreateAccountController.class);
	@Autowired
	private UserService userService;



	@RequestMapping(value = "createAccount")
	public String createAccount(Model model) {
		model.addAttribute(new CreateAccountFormBean());
        return CREATE_ACCOUNT_VIEW_NAME;
	}

	@AuditTrail("Create Account")
	@RequestMapping(value = "createAccount", method = RequestMethod.POST)
	public String createAccount(@Valid @ModelAttribute CreateAccountFormBean createAccountFormBean, Errors errors, RedirectAttributes ra,HttpServletRequest request,HttpServletResponse response,Locale locale) throws Exception{

		if (errors.hasErrors()) {
			return CREATE_ACCOUNT_VIEW_NAME;
		}

		Date now = UtilDates.currentDateTime();
		User user = createAccountFormBean.createAccount();
		user.setRole(getCurrentRoleFromUI(createAccountFormBean.getRole()));
		user.setName(new UserName(createAccountFormBean.getFirstname(), createAccountFormBean.getLastname()));
		user.setEmail(createAccountFormBean.getEmail());
		user.setStatus(AccountStatus.PENDING.getStatus());
		user.setCreatedOn(now);
		if(user.getUserNameAudits().isEmpty())
		   user.setUserNameAudits(Arrays.asList(new UserNameAudit(user.getUsername(), now)));
		else
		   user.getUserNameAudits().add(new UserNameAudit(user.getUsername(), now));

		/*Send user email invitation*/
		String token  = Token.generateToken(user.getUsername());
		int pass = Integer.valueOf(getConfigProperty("password.expire"));
		Date expireToken = Token.getNewValidationExpireDate(token, Integer.valueOf(pass));
		user.setToken(Token.encrypt(token));
		user.setExpireToken(expireToken);
		userService.create(user);
		if(user.getPasswordAudits().isEmpty())
		   user.setPasswordAudits(Arrays.asList(new TokenAudit(user.getToken(),expireToken,now)));
		else
		   user.getPasswordAudits().add(new TokenAudit(user.getToken(),expireToken,now));

		String pwdlink = request.getRequestURL().toString().replace("createAccount","show/firstTimeRegistration")+"/" + user.getToken();
		Map<String,String> var = new HashMap<String,String>();
		var.put("firstname",user.getName().getFirst());
		var.put("pwdLink",pwdlink);
		var.put("pwdexpireMsg", getMessage("password.expire.msg", locale, pass < 60 ? String.valueOf(pass) : String.valueOf(pass / 60)));
		var.put("emailSupport", getConfigProperty("email.support"));
		emailService.sendUserFirstTimeRegistrationEmail(request, response, getConfigProperty("email.support"), user.getEmail(), getMessage("first.time.email.subject", locale, null), var);
		/*End*/
		CustomMessageHelper.addSuccessAttribute(ra, "create.account.success",user.getUsername());
		return "redirect:/";
	}


}
