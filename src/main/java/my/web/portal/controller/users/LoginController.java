package my.web.portal.controller.users;

import my.web.portal.annotation.AuditTrail;
import my.web.portal.controller.AbstractController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by christopherloganathan on 6/14/15.
 */
@Controller
public class LoginController extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @AuditTrail("Login Account")
	@RequestMapping(value = "login")
	public String login() {
        return "login/login";
    }

}
