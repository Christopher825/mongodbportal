package my.web.portal.controller.users;

import my.web.portal.annotation.AuditTrail;
import my.web.portal.controller.AbstractController;
import my.web.portal.formbean.AccountFormBean;
import my.web.portal.model.user.TokenAudit;
import my.web.portal.model.user.User;
import my.web.portal.model.user.UserName;
import my.web.portal.model.user.UserNameAudit;
import my.web.portal.service.user.UserService;
import my.web.portal.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

/**
 * Created by christopherloganathan on 6/22/15.
 */
@Controller
public class AccountController extends AbstractController {

    private static final String ACCOUNT_VIEW_NAME = "account/viewAccount";

    private static final String ACCOUNT_LIST = "account/accountList";

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "viewAccountPost", method = RequestMethod.POST)
    @ResponseBody
    public String viewAccount(Model model,@RequestParam(value = "userID", required = false) String id,HttpSession session) {
        session.setAttribute(SessionAttribute.ID.getAttribute(),id);
        return "success";
    }

    @AuditTrail("View Account")
    @RequestMapping(value = "viewAccount")
    public String viewAccount(Model model,HttpSession session) {
        User user;
        String id = (String)session.getAttribute(SessionAttribute.ID.getAttribute());
        if(!CommonUtils.isEmpty(id)){
            user = userService.findUserByID(id);
        }else{
            user = AuthenticationUtils.getAuthenticatedUser();

        }
        AccountFormBean accountFormBean = new AccountFormBean();
        accountFormBean.setId(user.getId().toString());
        accountFormBean.setUsername(user.getUsername());
        accountFormBean.setFirstname(CommonUtils.isEmpty(user.getName()) ? "" : user.getName().getFirst());
        accountFormBean.setLastname(CommonUtils.isEmpty(user.getName()) ? "" : user.getName().getLast());
        accountFormBean.setEmail(user.getEmail());
        if(!CommonUtils.isEmpty(id) || user.isAdmin()){
            accountFormBean.setRole(getCurrentRoleFromSecurityConfig(user.getRole()));
        }else{
            accountFormBean.setRole(user.getRole());
        }
        model.addAttribute(accountFormBean);
        session.removeAttribute(SessionAttribute.ID.getAttribute());
        return ACCOUNT_VIEW_NAME;
    }

    @AuditTrail("Edit Account")
    @RequestMapping(value = "editAccount", method = RequestMethod.POST)
    public String editAccount(@Valid @ModelAttribute AccountFormBean accountFormBean, Errors errors,RedirectAttributes ra,Model model,Pageable pageable,Principal principal){

        User user = AuthenticationUtils.getAuthenticatedUser();
        String urlRedirect = "redirect:/";
        if(!accountFormBean.getId().equals(user.getId().toString())){
            user  = userService.findUserByID(accountFormBean.getId());
            if(!accountFormBean.getRole().equals(AccessRoles.ROLE_USER))
                user.setRole(getCurrentRoleFromUI(accountFormBean.getRole()));
            CustomMessageHelper.addSuccessAttribute(model, "edit.account.success", user.getUsername());
            urlRedirect = ACCOUNT_LIST;

        }else{
            CustomMessageHelper.addSuccessAttribute(ra, "edit.account.success", user.getUsername());
            if(!accountFormBean.getRole().equals(AccessRoles.ROLE_USER))
                user.setRole(getCurrentRoleFromUI(accountFormBean.getRole()));
        }

        if (errors.hasErrors()) {
                return ACCOUNT_VIEW_NAME;
        }

        Date now = UtilDates.currentDateTime();
        user.setUsername(accountFormBean.getUsername());
        user.setName(new UserName(accountFormBean.getFirstname(), accountFormBean.getLastname()));
        user.setEmail(accountFormBean.getEmail());
        user.setUpdatedOn(now);
        Collections.sort(user.getUserNameAudits(), new Comparator<UserNameAudit>() {
            @Override
            public int compare(UserNameAudit un1, UserNameAudit un2) {

                return un2.getAddedOn().compareTo(
                        un1.getAddedOn());
            }
        });
        if(!(user.getUserNameAudits().get(0).getUsername().equalsIgnoreCase(user.getUsername()))){
            user.getUserNameAudits().add(new UserNameAudit(user.getUsername(), now));
        }
        userService.update(user);
        if(urlRedirect.equalsIgnoreCase(ACCOUNT_LIST)){
            createUserPagination(pageable,principal,model,null);
        }
        return urlRedirect;

    }

    @AuditTrail("Search Account")
    @RequestMapping(value = "searchAccountPost", method = RequestMethod.POST)
    @ResponseBody
    public String accountList(@RequestParam String searchAccount,HttpSession session) {
        session.setAttribute(SessionAttribute.SEARCH_ACCOUNT.getAttribute(), searchAccount);
        session.setAttribute(SessionAttribute.PAGE_VIEW_SEARCH_ACCOUNT.getAttribute(),searchAccount);
        return "success";
    }

    @AuditTrail("View Account List")
    @RequestMapping(value = "accountList")
    public String accountList(Model model,Pageable pageable,Principal principal,HttpSession session,HttpServletRequest request) {

        String searchAccount = (String) session.getAttribute(SessionAttribute.SEARCH_ACCOUNT.getAttribute());
        if(!(CommonUtils.isEmpty(request.getParameter(RequestPageAttribute.Page_ITEM.getAttribute()))|| CommonUtils.isEmpty(request.getParameter(RequestPageAttribute.Page_SIZE.getAttribute()))))
            searchAccount = (String) session.getAttribute(SessionAttribute.PAGE_VIEW_SEARCH_ACCOUNT.getAttribute());
        else
            session.removeAttribute(SessionAttribute.PAGE_VIEW_SEARCH_ACCOUNT.getAttribute());

        model.addAttribute("searchAcct",searchAccount);
        createUserPagination(pageable,principal,model,searchAccount);
        session.removeAttribute(SessionAttribute.SEARCH_ACCOUNT.getAttribute());

        return ACCOUNT_LIST;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "testSearchAccountPost",method = RequestMethod.POST)
    @ResponseBody
    public String searchAccountPostTest(@RequestParam(value = "searchAccount") String searchAccount) throws Exception {
        User user = userService.findUserByUsername(CommonUtils.getDefaultStringValue(searchAccount,""));
        return CommonUtils.createGsonBuilder(CustomObjectSerializer.serializerObjectID(user));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "testShowAllAccountPost",method = RequestMethod.POST)
    @ResponseBody
    public String showAllAccountPostTest() throws Exception {
        List<User> users = userService.list();
        return CommonUtils.createGsonBuilder(CustomObjectSerializer.serializerObjectID(users));
    }


    @RequestMapping(value = "deleteAccountPost", method = RequestMethod.POST)
    @ResponseBody
    public String deleteAccount(@RequestParam String userID,HttpSession session) {
        session.setAttribute(SessionAttribute.ID.getAttribute(), userID);
        return "success";
    }

    @AuditTrail("Delete Account")
    @RequestMapping(value = "deleteAccount")
    public String deleteAccount(Model model,Pageable pageable,Principal principal,HttpSession session) {
        String id = (String)session.getAttribute(SessionAttribute.ID.getAttribute());
        User user = userService.findUserByID(id);
        String username = user.getUsername();
        userService.delete(user);
        createUserPagination(pageable,principal,model,null);
        session.removeAttribute(SessionAttribute.ID.getAttribute());
        CustomMessageHelper.addSuccessAttribute(model, "delete.account.success", username);
        return ACCOUNT_LIST;

    }

    @RequestMapping(value = "lockAccountPost",method = RequestMethod.POST)
    @ResponseBody
    public String lockAccount(@RequestParam String userID,@RequestParam boolean lock,HttpSession session) {
        session.setAttribute(SessionAttribute.ID.getAttribute(), userID);
        session.setAttribute(SessionAttribute.LOCK.getAttribute(), lock);
        return "success";
    }

    @AuditTrail("Lock Account")
    @RequestMapping(value = "lockAccount")
    public String lockAccount(Model model,Pageable pageable,Principal principal,HttpSession session) {

        String id = (String)session.getAttribute(SessionAttribute.ID.getAttribute());
        boolean lock = Boolean.valueOf(session.getAttribute(SessionAttribute.LOCK.getAttribute()).toString());
        User user = userService.findUserByID(id);
        String username = user.getUsername();
        if(lock == true){
            user.setStatus("active");
            CustomMessageHelper.addSuccessAttribute(model, "unlock.account.success",username);
        }else{
            user.setStatus("inactive");
            CustomMessageHelper.addSuccessAttribute(model, "lock.account.success",username);
        }
        userService.update(user);
        session.removeAttribute(SessionAttribute.ID.getAttribute());
        session.removeAttribute(SessionAttribute.LOCK.getAttribute());
        createUserPagination(pageable, principal, model, null);
        return ACCOUNT_LIST;
    }


    @RequestMapping(value = "reinviteAccountPost",method = RequestMethod.POST)
    @ResponseBody
    public String reinviteUserToRegister(@RequestParam String userID,HttpSession session) {
        session.setAttribute(SessionAttribute.ID.getAttribute(), userID);
        return "success";
    }

    @AuditTrail("Re-invite Account")
    @RequestMapping(value = "reinviteAccount")
    public String reinviteUserToRegister(Model model,Pageable pageable,Principal principal,HttpSession session,HttpServletRequest request,HttpServletResponse response,Locale locale) throws Exception {

      /*Send user email invitation*/
        String id = (String)session.getAttribute(SessionAttribute.ID.getAttribute());
        User user  = userService.findUserByID(id);
        String username = user.getUsername();
        Date now = UtilDates.currentDateTime();
        String token  = Token.generateToken(user.getUsername());
        int pass = Integer.valueOf(getConfigProperty("password.expire"));
        Date expireToken = Token.getNewValidationExpireDate(token, Integer.valueOf(pass));
        user.setToken(Token.encrypt(token));
        user.setExpireToken(expireToken);
        if(user.getPasswordAudits().isEmpty())
            user.setPasswordAudits(Arrays.asList(new TokenAudit(user.getToken(),expireToken,now)));
        else
            user.getPasswordAudits().add(new TokenAudit(user.getToken(),expireToken,now));

        String pwdlink = request.getRequestURL().toString().replace("reinviteAccount","show/firstTimeRegistration")+"/" + user.getToken();
        Map<String,String> var = new HashMap<String,String>();
        var.put("firstname",user.getName().getFirst());
        var.put("pwdLink",pwdlink);
        var.put("pwdexpireMsg", getMessage("password.expire.msg", locale, pass < 60 ? String.valueOf(pass) : String.valueOf(pass / 60)));
        var.put("emailSupport", getConfigProperty("email.support"));
        session.removeAttribute(SessionAttribute.ID.getAttribute());
        userService.update(user);
        emailService.sendUserFirstTimeRegistrationEmail(request, response, getConfigProperty("email.support"), user.getEmail(), getMessage("first.time.email.subject", locale, null), var);
		/*End*/
        CustomMessageHelper.addSuccessAttribute(model, "reinvite.account.success", username);
        createUserPagination(pageable,principal,model,null);
        return ACCOUNT_LIST;
    }

    private void createUserPagination(Pageable pageable,Principal principal,Model model,String search){

        PageWrapper<User> page = new PageWrapper<User>
                (userService.list(pageable,principal.getName(),search), "accountList");
        model.addAttribute("page", page);
    }





}
