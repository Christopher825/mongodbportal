package my.web.portal.controller;

import my.web.portal.config.PropertiesAccessor;
import my.web.portal.utils.AccessRoles;
import my.web.portal.utils.EmailService;
import my.web.portal.utils.ExceptionProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ModelAttribute;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by christopherloganathan on 6/16/15.
 */
public abstract class AbstractController {

    @Autowired
    private ExceptionProcessor exceptionProcessor;

    @Autowired
    protected EmailService emailService;

    @Autowired
    protected PropertiesAccessor urlProp;


    @Autowired
    protected  MessageSource messageSource;

    protected enum RequestPageAttribute {
        Page_ITEM("page.page"), Page_SIZE("page.size");

        public String getAttribute() {
            return attribute;
        }

        protected String attribute;

        RequestPageAttribute(String attribute) {
            this.attribute = attribute;
        }

    }


    protected void processException(Exception ex){
        exceptionProcessor.process(ex);
    }

    protected String getConfigProperty(String key){
        return urlProp.getProperty(key);
    }

    protected String getCurrentRoleFromSecurityConfig(String role){

        switch(role){

            case AccessRoles.ROLE_ADMIN:
                return "admin";

            case  AccessRoles.ROLE_USER:
                return "user";

            default:
                return "anonymous";
        }
    }

    protected String getCurrentRoleFromUI(String role){

        switch(role){

            case "admin":
                return AccessRoles.ROLE_ADMIN;

            case "user":
                return AccessRoles.ROLE_USER;

            default:
                return AccessRoles.ROLE_ANONYMOUS;
        }
    }

    @ModelAttribute("roles")
    protected Map roles() throws Exception {

        Map<String,String>  roles = new HashMap<String,String>();
        roles.put("admin", "ROLE_ADMIN");
        roles.put("user", "ROLE_USER");
        return roles;
    }

    protected String getMessage(String messageKey,Locale locale,String... messageParams) {
        return messageSource.getMessage(messageKey, messageParams,locale);
    }
}
