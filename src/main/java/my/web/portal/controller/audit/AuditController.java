package my.web.portal.controller.audit;

import my.web.portal.controller.AbstractController;
import my.web.portal.model.audit.Audit;
import my.web.portal.service.audit.AuditService;
import my.web.portal.utils.CommonUtils;
import my.web.portal.utils.PageWrapper;
import my.web.portal.utils.SessionAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by christopherloganathan on 15/03/2016.
 */
@Controller
public class AuditController extends AbstractController {

    @Autowired
    private AuditService auditService;

    private static final Logger logger = LoggerFactory.getLogger(AuditController.class);

    private static final String AUDIT_LIST = "audit/auditList";


    @RequestMapping(value = "searchAuditPost", method = RequestMethod.POST)
    @ResponseBody
    public String auditList(@RequestParam String startDate,@RequestParam String endDate, HttpSession session) {
        session.setAttribute(SessionAttribute.START_DATE_AUDIT.getAttribute(), startDate);
        session.setAttribute(SessionAttribute.END_DATE_AUDIT.getAttribute(), endDate);
        session.setAttribute(SessionAttribute.PAGE_VIEW_START_DATE_AUDIT.getAttribute(), startDate);
        session.setAttribute(SessionAttribute.PAGE_VIEW_END_DATE_AUDIT.getAttribute(), endDate);
        return "success";
    }


    @RequestMapping(value = "auditTrail")
    public String auditList(Model model, Pageable pageable, HttpSession session, HttpServletRequest request) {
        String startDate = (String) session.getAttribute(SessionAttribute.START_DATE_AUDIT.getAttribute());
        String endDate = (String) session.getAttribute(SessionAttribute.END_DATE_AUDIT.getAttribute());
        if(!(CommonUtils.isEmpty(request.getParameter(RequestPageAttribute.Page_ITEM.getAttribute()))|| CommonUtils.isEmpty(request.getParameter(RequestPageAttribute.Page_SIZE.getAttribute())))){
            startDate = (String) session.getAttribute(SessionAttribute.PAGE_VIEW_START_DATE_AUDIT.getAttribute());
            endDate = (String) session.getAttribute(SessionAttribute.PAGE_VIEW_END_DATE_AUDIT.getAttribute());
        }
        model.addAttribute("startDate",startDate);
        model.addAttribute("endDate",endDate);
        createAuditPagination(pageable,model,startDate,endDate);
        session.removeAttribute(SessionAttribute.START_DATE_AUDIT.getAttribute());
        session.removeAttribute(SessionAttribute.END_DATE_AUDIT.getAttribute());
        return AUDIT_LIST;
    }


    private void createAuditPagination(Pageable pageable,Model model,String startDate,String endDate){

        PageWrapper<Audit> page = new PageWrapper<Audit>
                (auditService.list(pageable,startDate,endDate), "auditTrail");
        model.addAttribute("page", page);
    }
}
