package my.web.portal.controller.home;

import my.web.portal.annotation.AuditTrail;
import my.web.portal.controller.AbstractController;
import my.web.portal.service.audit.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.security.Principal;


/**
 * Created by christopherloganathan on 6/14/15.
 */

@Controller
public class HomeController extends AbstractController {

    @Autowired
    private AuditService auditService;

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @AuditTrail("Home Access")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Principal principal) {

        return "home/home";
    }


}
