package my.web.portal.model.user;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

/**
 * Created by christopherloganathan on 6/25/15.
 */
public final class TokenAudit {


    @Getter @Setter
    private String token;
    @Getter @Setter private Date addedOn;
    @Getter @Setter private Date expireToken;

    public TokenAudit(String token, Date expireToken, Date addedOn){
        this.addedOn = addedOn;
        this.token = token;
        this.expireToken = expireToken;
    }
}
