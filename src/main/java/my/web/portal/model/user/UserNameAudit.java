package my.web.portal.model.user;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

/**
 * Created by christopherloganathan on 6/24/15.
 */
public final class UserNameAudit {

    @Getter
    @Setter
    private String username;
    @Getter @Setter private Date addedOn;

    public UserNameAudit(String username, Date addedOn){
        this.username = username;
        this.addedOn = addedOn;
    }
}
