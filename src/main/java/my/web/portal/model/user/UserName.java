package my.web.portal.model.user;

import lombok.Getter;
import lombok.Setter;


/**
 * Created by christopherloganathan on 6/22/15.
 */
public final class UserName {

    @Getter @Setter private String first;
    @Getter @Setter private String last;

    public UserName(String first, String last){
        this.first = first;
        this.last  =last;
    }
}
