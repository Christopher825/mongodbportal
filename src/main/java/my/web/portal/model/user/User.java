package my.web.portal.model.user;

/**
 * Created by christopherloganathan on 6/14/15.
 */

import lombok.Getter;
import lombok.Setter;
import my.web.portal.model.Base;
import my.web.portal.utils.AccessRoles;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.*;


@Document (collection="user")
public final class User extends Base implements UserDetails{

	@Setter @Getter private String username;
	@Setter @Getter private UserName name;
	@Setter @Getter private String role;
	@Setter @Getter private String email;
	@JsonIgnore
	@Setter @Getter private String password;
	@Setter @Getter private String token;
	@Setter @Getter private Date expireToken;
	@Setter @Getter private List<TokenAudit> passwordAudits = new ArrayList<TokenAudit>();
	@Setter @Getter private String status;
	@Setter @Getter private List<UserNameAudit> userNameAudits = new ArrayList<UserNameAudit>();
	@Setter @Getter private Date createdOn;
	@Setter @Getter private Date updatedOn;

	
	public User(String username,String role,String status) {
		this.username = username;
		this.role = role;
		this.status = status;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(new SimpleGrantedAuthority(getRole()));
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return status.equalsIgnoreCase("active");
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


	public boolean isAdmin() {
        return AccessRoles.ROLE_ADMIN.equals(getRole());
    }
}
