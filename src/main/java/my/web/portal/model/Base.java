package my.web.portal.model;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Created by christopherloganathan on 6/14/15.
 */
public abstract class Base implements Serializable {

    private static final long serialVersionUID = -2330901863611400652L;
    @Id
    @Getter @Setter
    protected ObjectId id = new ObjectId();
}
