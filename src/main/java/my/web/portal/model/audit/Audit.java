package my.web.portal.model.audit;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;

/**
 * Created by christopherloganathan on 15/03/2016.
 */
@Document(collection="audit")
public final class Audit {

    @Setter @Getter private String username;
    @Setter @Getter private String action;
    @Setter @Getter private String accessLevel;
    @Setter @Getter private Date createdOn;
}
