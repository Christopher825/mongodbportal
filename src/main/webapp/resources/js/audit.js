$(function () {
	$('#startDate').datetimepicker();
	$('#endDate').datetimepicker();
	$("#startDate").on("dp.change",function (e) {
        $('#endDate').data("DateTimePicker").setMinDate(e.date);
	});
	$("#endDate").on("dp.change",function (e) {
        $('#startDate').data("DateTimePicker").setMaxDate(e.date);
	});


    $('#viewAudit').click(function(event)
        {
            $.ajax({
                     url: './searchAuditPost',
                     type: 'post',
                     data: {
                     startDate: $('#startDt').val(),
                     endDate: $('#endDt').val()
                     },
                     success: function (data) {
                         if(data == 'success')
                                $(location).attr('href', "./auditTrail");
                     }


             });
        });
});


