   function deleteAccount(userID){

        $.ajax({
             url: './deleteAccountPost',
             type: 'post',
             data: {
                userID: userID
              },
             success: function (data) {
                 if(data == 'success')
                      $(location).attr('href', "./deleteAccount");
              }
        });
   }

   function lockAccount(userID,lock){

           $.ajax({
                   url: './lockAccountPost',
                   type: 'post',
                   data: {
                      userID: userID,lock: lock
                    },
                   success: function (data) {
                      if(data == 'success')
                           $(location).attr('href', "./lockAccount");
                   }
          });

   }

   function viewAccount(userID){

        $.ajax({
                     url: './viewAccountPost',
                     type: 'post',
                     data: {
                          userID: userID
                     },
                     success: function (data) {
                           if(data == 'success')
                                $(location).attr('href', "./viewAccount");
                     }
              });
   }

   function resendPasswordLink(userID){


        $.ajax({
              url: './reinviteAccountPost',
              type: 'post',
              data: {
                   userID: userID
              },
              success: function (data) {
                    if(data == 'success')
                         $(location).attr('href', "./reinviteAccount");
              }
       });
    }



     $(function() {
        $('#searchUser').bind("enterKey",function(){

           $.ajax({
               url: './searchAccountPost',
               type: 'post',
               data: {
                     searchAccount: $('#searchUser').val()
               },
               success: function (data) {
                    if(data == 'success')
                        $(location).attr('href', "./accountList");
               }
           });

        });
        $('#searchUser').keyup(function(e){
        if(e.keyCode == 13)
        {
          $(this).trigger("enterKey");
        }
        });
    });


