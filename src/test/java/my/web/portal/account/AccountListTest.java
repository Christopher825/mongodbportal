package my.web.portal.account;

import my.web.portal.config.AspectConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;



/**
 * Created by christopherloganathan on 6/14/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AspectConfig.class})
@WebAppConfiguration
public class AccountListTest  {

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilter;


    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(wac).addFilter(springSecurityFilter).alwaysDo(print()).build();

    }

    @Test
    public void runAllTest() throws Exception {

        String username = System.getProperty("uname");
        String password = System.getProperty("pwd");
        shouldReturnExpectedSearchedUserAsAdminLogin(username,password);
        shouldReturnExpectedAllUsersAsAdminLogin(username,password);
    }



    public void shouldReturnExpectedSearchedUserAsAdminLogin(String username,String password) throws Exception {

        Authentication auth =
                new UsernamePasswordAuthenticationToken(username, password);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);

        MockHttpSession sess = new MockHttpSession();
        sess.setAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                sc);

        mockMvc.perform(post("/testSearchAccountPost").session(sess).param("searchAccount",username))
                                             .andExpect(status().isOk())
                                              .andExpect(content().string(containsString("ROLE_ADMIN")));

    }


    public void shouldReturnExpectedAllUsersAsAdminLogin(String username,String password) throws Exception {

        Authentication auth =
                new UsernamePasswordAuthenticationToken(username, password);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);

        MockHttpSession sess = new MockHttpSession();
        sess.setAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                sc);

        mockMvc.perform(post("/testShowAllAccountPost").session(sess))
                .andExpect(status().isOk());

    }

}
