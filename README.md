# README #

Mini sample of MongoDB Web App
### What is this repository for? ###

* This web app consist of pure Mongo DB APIs covered several security aspects such as XSS, CSRF and etc. It's an agile startup for web app analytical dashboards with user access management using mongoDB as data source. Currently the web app consists of user management and audit trail modules. Additional MQ service available if need to push message to the web app and. Email service available currently for user management notifications.

* **Web technologies used:** Spring MVC 4.X, Spring Security 3.X and Thymeleaf 2.X 

* **JDK used:** 1.7

* **Maven Artifact Version:** 1.0.0-BUILD-SNAPSHOT
  
### How do I get set up? ###

* **Pre Installations:**
  Mongo DB 3.2 (WiredTiger storage engine)
  ,Active MQ 5.13.2 and Maven 3.3.9

* **Spring property placeholder configuration:**
  Initial admin user with default encoded password will be available in Mongo DB database[app] and collection[user]. This initial admin user will be created upon web app spring mvc startup. However, in order to access as first admin user via login page of the web app, the plain text password is available in portal.properties located in classpath resources.

* **How to run tests:**
Execute command[mvn clean install -Duname=<username> -Dpwd=<password>] to initialize spring mvc mockito test and build the project. The [-Duname] property must be an admin user account. To skip the test, execute command[mvn clean install -Dmaven.test.skip=true] to build the project only.

* **Deployment instructions:**
To deploy web app, execute command[mvn tomcat7:run]

### Who do I talk to? ###

* **Admin :** Christopher Loganathan
* **Email contact :** christopherloganathan@gmail.com